package zifuchuan.q5;

/**
 * 验证回文串
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 * 说明：本题中，我们将空字符串定义为有效的回文串。
 * 输入: "A man, a plan, a canal: Panama"
 * 输出: true
 * 解释："amanaplanacanalpanama" 是回文串
 *
 * 思路：先把数字+字母取出，然后全部小写，再进行比较
 *
 */
public class Solution {
    public boolean isPalindrome(String s) {
        StringBuilder sb = new StringBuilder();
        //遍历字符串，把大写+小写+数字取出，放入sb中
        for (int i = 0; i < s.length(); i++) {
            if((s.charAt(i) >= 48 && s.charAt(i) <=57) ||
                    (s.charAt(i) >= 65 && s.charAt(i) <= 90) ||
                    (s.charAt(i) >= 97 && s.charAt(i) <=122 )){
                sb.append(s.charAt(i));
            }
        }
        //将sb转成String，然后全部变成小写
        String s1 = sb.toString();
        String s2 = s1.toLowerCase();
        //遍历新的字符串，到长度一半即可，判断头尾元素是否相同，有不同元素直接返回false，遍历完成没有不等的，返回true
        for (int i = 0; i < s2.length()/2; i++) {
            if(s2.charAt(i) != s2.charAt(s2.length() - 1 - i)){
                return false;
            }
        }
        return true;
    }
}
