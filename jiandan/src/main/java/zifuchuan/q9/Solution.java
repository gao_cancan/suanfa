package zifuchuan.q9;

/**
 * 最长公共前缀
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * 如果不存在公共前缀，返回空字符串 ""。
 * 1 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 * strs[i] 仅由小写英文字母组成
 *
 * 思路：先找到数组中，最短字符串的长度length
 * 双重for循环，外层是1-length，表示获取每个String的长度，内层是1-strs.length，表示获取strs[]中每个元素String，
 * 并比较相邻两个元素
 */
public class Solution {
    public String longestCommonPrefix(String[] strs) {
        if(strs.length == 1){
            return strs[0];
        }
        int length = strs[0].length();
        for (int i = 1; i < strs.length; i++) {
            if(strs[i].length() < length){
                length = strs[i].length();
            }
        }
        String s = "";
        boolean b = false;
        for (int i = 1; i <= length; i++) {
            for (int j = 1; j < strs.length; j++) {
                if(!strs[j].substring(0,i).equals(strs[j-1].substring(0,i))){
                    b = true;
                    break;
                }
            }
            s = strs[0].substring(0,i-1);
            if(b){
                s = strs[0].substring(0,i);
                break;
            }
        }
        return s;
    }

    public static void main(String[] args) {
//        String s1 = "aaa";
//        String substring = s1.substring(0, 0);
//        System.out.println(substring);
//        String[] strs = {"flower","flow","flight"};
        String[] strs = {"ab","a"};
        String s = new Solution().longestCommonPrefix(strs);
        System.out.println(s);
    }
}
