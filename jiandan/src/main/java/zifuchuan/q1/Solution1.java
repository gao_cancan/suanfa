package zifuchuan.q1;

/**
 * 反转字符串
 * 思路：头元素下标0，尾元素下标s.length-1,它们的和是s.length-1。
 * 反转字符串，交换的元素下标和是s.length-1，并且只需要遍历到 s.length / 2即可。（前一半与后一半交换完，不就是全反转吗）
 */
public class Solution1 {
    public void reverseString(char[] s) {
        //遍历前一半元素
        for (int i = 0; i < s.length/2; i++) {
            //交换元素的下标和是 s.length -1。所以交换元素的下标分别是i 和 s.length-1-i
            char temp = s[i];
            s[i] = s[s.length-1-i];
            s[s.length-1-i] = temp;
        }
    }
}
