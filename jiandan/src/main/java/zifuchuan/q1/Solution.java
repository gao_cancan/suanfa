package zifuchuan.q1;

/**
 * 反转字符串
 * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
 * 不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
 *
 * 思路：双指针，头指针head,尾指针tail,相互交换，然后head向后，tail向前，一直交换，直到tail < head，遍历结束
 */
public class Solution {
    public void reverseString(char[] s) {
        //头指针，指向下标0处
        int head = 0;
        //尾指针，指向下标s.length-1处
        int tail = s.length-1;
        //当tail >= head时，说明还没有遍历完，继续遍历
        while(tail >= head){
            //交换两个元素，并将head后移一位，tail前移一位
            char temp = s[head];
            s[head] = s[tail];
            s[tail] = temp;
            head++;
            tail--;
        }
    }
}
