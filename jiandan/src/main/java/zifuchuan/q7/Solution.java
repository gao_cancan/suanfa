package zifuchuan.q7;

/**
 * 实现 strStr() 函数。
 * 给你两个字符串 haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串出现的第一个位置（下标从 0 开始）。如果不存在，则返回  -1 。
 * 说明：
 * 当 needle 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
 * 对于本题而言，当 needle 是空字符串时我们应当返回 0 。这与 C 语言的 strstr() 以及 Java 的 indexOf() 定义相符。
 *
 * 思路：1、如果needle为null 或长度为0，直接返回0
 * 2、遍历haystack，从0到haystack.length()-needle.length()，截取needle.length长度的字符串，与needle比较，如果有，则返回下标
 * 最后遍历完，没有相等的，则返回-1
 * i<=haystack.length()-needle.length()。超过该下标后，haystack的剩余长度比needle的长度短，肯定不会有符合要求的字符串了。
 */
public class Solution {
    public int strStr(String haystack, String needle) {
        if(needle == null || needle.length() == 0){
            return 0;
        }

        for (int i = 0; i <= haystack.length()-needle.length(); i++) {
            String substring = haystack.substring(i, i+needle.length());
            if(substring.equals(needle)){
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int i = new Solution().strStr("", "a");
        System.out.println(i);
    }
}
