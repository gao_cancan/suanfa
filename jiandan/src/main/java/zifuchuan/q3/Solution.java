package zifuchuan.q3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 字符串中的第一个唯一字符
 * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。
 * 提示：你可以假定该字符串只包含小写字母。
 * 思路：
 *
 *
 * 1、String转char[]的方法：char[] chars = str.toCharArray();
 * 2、char怎么比较大小：char本来就是ACSII码，实质上就是数字，数字怎么比较他就怎么比较，直接==
 *
 *
 * 总结：
 * 第一次思路有问题：先将String转成char[]，遍历char[]，使用双重for，将每一个元素，依次与其后面的元素进行比较，
 * 如果没有相等的，则返回其下标
 * 如果有相等的，继续遍历下一个，直到遍历所有都没有相等的，则返回-1
 *
 * 问题在于：比如字符串aabb，当第一次遍历a不符合，但遍历到第二个a时，a后面就没有相同元素了，就认为应该返回下标1，这是有问题的
 * 如果能排除掉：已经使用过的相同元素值，我想的是用map，看containsKey
 *
 * 第二次思路有问题：我一直在想hashMap是无序的，下标、元素值、出现次数，怎么放到map中，但是我忘了一个重要的点：String还在，它是不变的。
 * 我把元素值和出现次数放到下标中，不用改map的无序，再遍历一下char[],char[]的顺序没有变呀，得到==的值，返回其下标，没有相等的返回-1
 */

/**
 * 错误答案
 */
public class Solution {
    public int firstUniqChar(String s) {
        char[] chars = s.toCharArray();
        Map<Character,Integer> map = new HashMap<>();
        for (int i = 0; i < chars.length; i++) {
            boolean value = true;
            if(map.containsKey(chars[i])){
                break;
            }else{
                map.put(chars[i],i);
            }

            for (int j = i+1; j < chars.length; j++) {
                if(chars[i] == chars[j]){
                    value = false;
                }
            }
            if(value){
                return i;
            }
        }
        return -1;

    }


    public static void main(String[] args) {
        int aabb = new Solution().firstUniqChar("dddccdbba");
        System.out.println(aabb);
    }
}
