package zifuchuan.q3;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * 字符串中的第一个唯一字符
 * 思路：
 *
 * todo  补充思路
 */
public class Solution2 {
    public int firstUniqChar(String s) {
        Map<Character,Integer> map = new HashMap<>();
        Queue<Pair> queue = new LinkedList<>();
        int length = s.length();
        //遍历数组，放到map和queue中
        for (int i = 0; i < length; i++) {
            char ch = s.charAt(i);
            //如果map里没有，值为key，下标为value放到map中，也放到队列里，对象属性：值+下标
            if(!map.containsKey(ch)){
                map.put(ch,i);
                queue.offer(new Pair(ch,i));
            }else{
                //如果map里已包含，值为key，下标为-1，放到map中
                map.put(ch,-1);
                //删除队列里队头元素下标为-1的元素
                while(!queue.isEmpty() && map.get(queue.peek().ch) == -1){
                    queue.poll();
                }
            }
        }
        return queue.isEmpty() ? -1 : queue.poll().pos;
    }

    class Pair{
        char ch;
        int pos;

        public Pair(char ch, int pos) {
            this.ch = ch;
            this.pos = pos;
        }
    }
}
