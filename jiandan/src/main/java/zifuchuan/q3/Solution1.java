package zifuchuan.q3;

import java.util.HashMap;
import java.util.Map;

/**
 * 字符串中的第一个唯一字符
 * 思路：1、先将字符串转成char[]，遍历数组，把value作为key，出现次数作为value，放到map中
 * 2、再遍历char[]，看元素出现次数是否为1，为1则返回下标，如果没有，则返回-1
 */
public class Solution1 {
    public int firstUniqChar(String s) {
        //String转成char数组
        char[] chars = s.toCharArray();
        Map<Character,Integer> map = new HashMap<>();
        //遍历数组，key为元素值，value为出现次数
        for (int i = 0; i < chars.length; i++) {
            if(map.containsKey(chars[i])){
                int value = map.get(chars[i]);
                map.put(chars[i],++value);
            }else{
                map.put(chars[i],1);
            }
        }
        //遍历数组，看元素值中出现次数为1的，返回其下标
        for (int i = 0; i < chars.length; i++) {
            if(map.get(chars[i]) == 1){
                return i;
            }
        }
        //遍历结束，没有次数为1的，返回-1
        return -1;
    }
}
