package zifuchuan.q4;

import java.util.Arrays;

/**
 * 有效的字母异位词
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 * 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。
 *
 * 思路：排序
 * 1、申请两个数组，遍历两个String，分别放到char[]中
 * 2、对char[]进行排序
 * 3、遍历char[]，依次比较每个值，不同返回false
 * 4、遍历结束，没有不同，返回true
 */
public class Solution1 {
    public boolean isAnagram(String s, String t) {
        int sLength = s.length();
        int tLength = t.length();
        if(sLength != tLength){
            return false;
        }
        char[] sChars = new char[sLength];
        char[] tChars = new char[sLength];
        for (int i = 0; i < sLength; i++) {
            sChars[i] = s.charAt(i);
        }
        for (int i = 0; i < sLength; i++) {
            tChars[i] = t.charAt(i);
        }

        Arrays.sort(sChars);
        Arrays.sort(tChars);

        for (int i = 0; i < sLength; i++) {
            if(sChars[i] != tChars[i]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        boolean anagram = new Solution1().isAnagram("anagram", "nagaram");
        System.out.println(anagram);
    }
}
