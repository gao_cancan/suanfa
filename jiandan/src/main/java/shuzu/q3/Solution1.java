package shuzu.q3;

import java.util.Arrays;

/**
 * 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。
 * 元素交换的思路：
 *
 * todo   补充思路
 */
public class Solution1 {
    public void rotate(int[] nums, int k) {
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int temp = nums[i];
            nums[i] = nums[(i+k)%length];
            nums[(i+k)%length] = temp;
        }
        System.out.println(Arrays.toString(nums));
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7};
        int k = 3;
        new Solution1().rotate(nums,k);
    }
}
