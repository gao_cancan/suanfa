package shuzu.q3;

import java.util.Arrays;

/**
 * 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。
 * 示例：
 * 输入: nums = [1,2,3,4,5,6,7], k = 3
 * 输出: [5,6,7,1,2,3,4]
 * 解释:
 * 向右旋转 1 步: [7,1,2,3,4,5,6]
 * 向右旋转 2 步: [6,7,1,2,3,4,5]
 * 向右旋转 3 步: [5,6,7,1,2,3,4]
 *
 * 思路：这就是一个数组的内循环，首先新申请一个数组，用于存储移动后的元素。移动的规律是：
 * (下标i+移动量k) % 原数组长度length = 原数组中元素下标i
 */
class Solution {
    public void rotate(int[] nums, int k) {
        int length = nums.length;
        int[] array = new int[length];
        System.arraycopy(nums,0,array,0,length);


        for (int i = 0; i < length; i++) {
            nums[(i+k) % length] = array[i];
        }
        System.out.println(Arrays.toString(nums));
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7};
        int k = 3;
        new Solution().rotate(nums,k);

    }
}
