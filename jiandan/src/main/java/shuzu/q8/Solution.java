package shuzu.q8;

import java.util.Arrays;

/**
 * 移动零
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序
 * 要求：必须在原数组上操作，不能拷贝额外的数组。   尽量减少操作次数。
 *
 * 思路：双指针思路，head指向下标0处，tail指向下标length-1处。当tail < head，说明对所有元素进行了扫描，结束循环
 * 1、当tail指针为0，说明尾元素为0，tail指针向前移动一位
 * 2、tail指针不为0，当head指针为0，说明头元素为0，对head+1到tail间元素向前移动一位，然后将tail指针置为0，再将tail指针向前移动1位
 * 3、当tail和head指针都不为0，head指针向后移动1位
 */
public class Solution {
    public void moveZeroes(int[] nums) {
        //头指针
        int head = 0;
        //尾指针
        int tail = nums.length - 1;
        //如果长度为0或者1，直接返回
        if(nums.length ==0 || nums.length == 1){
            return;
        }
        //当tail指针 >= head指针，两指针还没有碰头，说明还没有遍历完，可以继续遍历
        while(tail >= head){
            //tail指针为0，则tail指针向前移动一位
            if(nums[tail] == 0){
                tail--;
                //tail指针不为0，head指针为0，把
            }else if(nums[head] == 0){
                for(int i=head;i<tail;i++){
                    nums[i] = nums[i+1];
                }
                nums[tail] = 0;
                tail--;
            }else{
                head++;
            }
        }
        System.out.println(Arrays.toString(nums));
    }


    public static void main(String[] args) {
        int[] nums = {0,0,1};
        new Solution().moveZeroes(nums);
    }
}
