package shuzu.q9;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 两数之和
 * 思路：哈希表
 * 1、遍历数组，当map里包含 target - nums[i] 时，返回下标i 和 map.get(target-nums[i])下标
 * 2、不包含时，把值nums[i]作为key，下标i作为value放入到map中
 */
public class Solution1 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        //遍历数组
        for (int i = 0; i < nums.length; i++) {
            //当map中包含target-nums[i],说明找到了，返回他们的下标
            if(map.containsKey(target-nums[i])){
                return new int[]{i,map.get(target-nums[i])};
            }
            map.put(nums[i],i);
        }
        return null;
    }
}
