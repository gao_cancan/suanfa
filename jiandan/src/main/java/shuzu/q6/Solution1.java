package shuzu.q6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 两个数组的交集 II
 * 给定两个数组，编写一个函数来计算它们的交集。
 * 思路：使用双指针
 * 1、先对数组排序
 * 2、定义两个变量（指针），分别指向两个数组，对数组进行遍历，遍历次数为两个数组长度和。
 * 3、获取数组值，value1 > value2，key2指针后移，value1 < value2，key1指针后移，相等则存入list中，并将两个指针都后移
 * 4、遍历list，放入到数组，并返回该数组
 */
public class Solution1 {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> list = new ArrayList<>();
        int key1 = 0;
        int key2 = 0;
        int length = nums1.length + nums2.length;
        for (int i = 0; i < length; i++) {
            if(key1 ==nums1.length || key2 == nums2.length){
                break;
            }
            int value1 = nums1[key1];
            int value2 = nums2[key2];
            if(value1 > value2){
                key2++;
            }else if(value1 < value2){
                key1++;
            }else {
                list.add(value1);
                key1++;
                key2++;
            }
        }
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] nums1 = {0,5,8,7,2,9,7,5};
        int[] nums2 = {1,4,8,9};
        int[] intersect = new Solution1().intersect(nums1, nums2);
        System.out.println(Arrays.toString(intersect));
    }
}
