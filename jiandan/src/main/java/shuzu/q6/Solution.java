package shuzu.q6;

import java.util.*;

/**
 * 两个数组的交集 II
 * 给定两个数组，编写一个函数来计算它们的交集。
 * 思路：
 * 1、申请两个map，分别遍历两个数组，把元素值作为key和出现次数作为value。
 * 2、遍历map1，如果map2里包含key，则比较他们的value（元素出现次数），取value值小的，放到新的map3中
 * 3、因为不知道数组长度，所以先用list去接收。遍历map3，key是元素值，value是出现次数，for循环value，
 * 把key放到list中
 * 4、最后遍历list，放到新数组中。
 */
public class Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer,Integer> map1 = new HashMap<>();
        Map<Integer,Integer> map2 = new HashMap<>();
        Map<Integer,Integer> map3 = new HashMap<>();
        List<Integer> list = new ArrayList<>();

        //遍历nums1数组，元素值为key，出现次数为value
        for (int i = 0; i < nums1.length; i++) {
            if(map1.containsKey(nums1[i])){
                Integer integer = map1.get(nums1[i]);
                map1.put(nums1[i],++integer);
            }else{
                map1.put(nums1[i],1);
            }
        }
        //遍历nums2数组，元素值为key，出现次数为value
        for (int i = 0; i < nums2.length; i++) {
            if(map2.containsKey(nums2[i])){
                Integer integer = map2.get(nums2[i]);
                map2.put(nums2[i],++integer);
            }else{
                map2.put(nums2[i],1);
            }
        }

        Set<Map.Entry<Integer, Integer>> entries = map1.entrySet();

        for (Map.Entry<Integer, Integer> entry : entries) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(map2.containsKey(key)){
                if(value <= map2.get(key)){
                    map3.put(key,value);
                }else{
                    map3.put(key,map2.get(key));
                }
            }
        }

        Set<Map.Entry<Integer, Integer>> entries1 = map3.entrySet();
        for (Map.Entry<Integer, Integer> entry : entries1) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            for (Integer i = 0; i < value; i++) {
                list.add(key);
            }
        }

        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] num1 = {4,9,5};
        int[] num2 = {9,4,9,8,4};
        int[] intersect = new Solution().intersect(num1, num2);
        System.out.println(Arrays.toString(intersect));
    }
}
