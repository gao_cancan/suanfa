package shuzu.q2;

/**
 * 买卖股票的最佳时机 II
 *
 * 给定一个数组 prices ，其中 prices[i] 是一支给定股票第 i 天的价格。
 * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
 * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
 *
 * 题目分析：数组从左到右，两数相减（右边的减左边的），得到的正数的总和
 * 举例：[7,1,5,3,6,4]，5-1=4,6-3=3，总和是4+3=7，输出7
 *
 * 思路：求的是所有差值（必须正数）的总和，那我采用双指针思路，A指针+B指针，A指针从下标0开始，B指针从A指针的下个元素开始
 * 当 B指针指向的值 - A指针指向的值 > 0，说明是负数，跳出循环，AB指针同时向前移动一位。
 * 当 B指针指向的值 - A指针指向的值 < 0，说明是正数，加到sum中，再跳出循环，AB指针同时向前移动一位。
 * 最后返回sum值
 */
class Solution {
    public int maxProfit(int[] prices) {
        //获取数组长度，如果为1，则没有差值，直接返回0
        int length = prices.length;
        if(length == 1){
            return 0;
        }
        //定义变量，总和sum
        int sum = 0;
        //遍历数组
        for (int i = 0; i < length; i++) {
            for(int j = i+1; j< length; j++){
                if(prices[j] < prices[i]){
                    break;
                }else{
                    sum += prices[j] - prices[i];
                    break;
                }
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] array = {7,1,5,3,6,4};
        int i = new Solution().maxProfit(array);
        System.out.println(i);
    }
}
