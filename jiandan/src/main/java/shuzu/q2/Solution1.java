package shuzu.q2;

/**
 * 买卖股票的最佳时机 II
 * 官方思路：其实求的就是相邻两数之差，大于0，则加总到sum中，最后返回sum
 */
public class Solution1 {
    public int maxProfit(int[] prices) {
        //获取数组长度，如果为1，则没有差值，直接返回0
        int length = prices.length;
        if(length == 1){
            return 0;
        }
        //定义变量，总和sum
        int sum = 0;
        //遍历数组
        for (int i = 1; i < length; i++) {
            //当后一个元素值 > 前一个元素值时，把差值加总到sum中
            if(prices[i] > prices[i-1]){
                sum += prices[i] - prices[i-1];
            }
        }
        return sum;
    }
}
