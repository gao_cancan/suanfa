package shuzu.q5;

/**
 * 只出现一次的数字
 * 思路：用了位运算-异或，
 * 异或的特点：1、任何值与0做异或，那么结果还是他本身
 * 2、任何数和自身做异或，结果是0
 * 3、异或满足交换律和结合律。a ^ b ^ a = b ^ a ^ a = b ^ (a ^ a) = b ^ 0 = b
 * 所以对数组元素做异或操作，获得的结果就是 只出现一次的数字
 */
public class Solution1 {
    public int singleNumber(int[] nums) {
        int value = nums[0];
        for (int i = 1; i < nums.length; i++) {
            value = value ^ nums[i];
        }
        return value;
    }

    public static void main(String[] args) {
        int[] nums = {2,1,2};
        int i = new Solution().singleNumber(nums);
        System.out.println(i);
    }
}
