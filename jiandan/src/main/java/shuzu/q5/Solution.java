package shuzu.q5;

import java.util.HashMap;
import java.util.Map;

/**
 * 只出现一次的数字
 * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 *
 * 思路：使用hash表，key是元素值，value是出现次数，最后遍历hash表，找出value是1的key即可
 */
public class Solution {
    public int singleNumber(int[] nums) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            //只会出现1次/2次
            if(map.containsKey(nums[i])){
                map.put(nums[i],2);
            }else{
                map.put(nums[i],1);
            }
        }
        int mapKey = 0;
        for (Map.Entry<Integer, Integer> integerIntegerEntry : map.entrySet()) {
            Integer key = integerIntegerEntry.getKey();
            if(map.get(key) == 1){
                mapKey = key;
            }
        }
        return mapKey;
    }

    public static void main(String[] args) {
        int[] nums = {2,1,2};
        int i = new Solution().singleNumber(nums);
        System.out.println(i);
    }
}
