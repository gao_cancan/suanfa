package shuzu.q7;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 *
 * 思路：
 * 1、倒序遍历数组digits，对数组最后一个元素+1，然后对它 % 10，如果结果为0，说明需要进1，继续相同操作
 * 如果 % 10 不为0，说明不用进1为，结束，返回digits
 * 2、如果没有结束，说明最高位需要再进1，新数组长度在原来基础上+1，最高位为1，返回digits
 */
public class Solution {
    public int[] plusOne(int[] digits) {
        for (int i = digits.length-1; i >= 0; i--) {
            digits[i]++;
            digits[i] = digits[i] % 10;
            if(digits[i] != 0) return digits;
        }

        digits = new int[digits.length+1];
        digits[0] = 1;
        return digits;
    }

    public static void main(String[] args) {
        int[] digits = {4,3,2,1};
        int[] ints = new Solution().plusOne(digits);
        System.out.println(Arrays.toString(ints));
    }
}
