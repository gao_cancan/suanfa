package shuzu.q4;

/**
 * 存在重复元素
 * 给定一个整数数组，判断是否存在重复元素。
 * 如果存在一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false
 *
 * 思路：双重for遍历，外层for指定某个值，依次与后面的元素进行比较。
 * 结果：提交反馈超时
 */
class Solution {
    public boolean containsDuplicate(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int key = nums[i];
            for (int j = i+1; j < nums.length; j++) {
                if(nums[j] == key){
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4};
        boolean b = new Solution().containsDuplicate(nums);
        System.out.println(b);
    }
}
