package shuzu.q4;

import java.util.Arrays;

/**
 * 存在重复元素
 * 重复返回true，没有重复元素返回false
 * 思路：先排序，排序完成后依次遍历，看相邻元素值有没有相等的，有则返回true，遍历完数组都没有相等的，则返回false
 */
public class Solution2 {
    public boolean containsDuplicate(int[] nums) {
        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++) {
            if(nums[i] == nums[i-1]){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4};
        boolean b = new Solution().containsDuplicate(nums);
        System.out.println(b);
    }
}
