package shuzu.q4;

import java.util.HashMap;
import java.util.Map;

/**
 * 存在重复元素
 * 重复返回true，没有重复元素返回false
 * 思路：使用hash表，key是数组值(看它是否重复)，value是下标，这样遍历一轮就可以知道是否有重复元素，时间复杂度为n
 */
public class Solution1 {
    public boolean containsDuplicate(int[] nums) {
        Map<Integer,Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            if(map.containsKey(nums[i])){
                return true;
            }else{
                map.put(nums[i],i);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4};
        boolean b = new Solution().containsDuplicate(nums);
        System.out.println(b);
    }
}
