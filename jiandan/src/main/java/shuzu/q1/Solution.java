package shuzu.q1;

/**
 * 删除排序数组中的重复项
 *
 * 思路：这是一个有序数组，我们使用快慢指针的思路。
 * 1、如果数组长度是0，直接返回0即可
 * 2、如果数组长度>0，那么最后返回的数组，一定是有值的
 * nums[0]是肯定不重复的，然后我们设定快慢指针，指向下标1的位置，让快指针从下标1到n-1，依次遍历。
 * 当快指针fast对应的值nums[fast] 与 前一个值nums[fast-1]不等时，把它赋值给nums[slow]，然后将slow+1;
 * fast指针每次都向前移动，所以它的值一直进行+1操作
 */
class Solution {
    public int removeDuplicates(int[] nums) {
        //如果数组长度为0，直接返回0
        if(nums.length == 0){
            return 0;
        }
        //快指针，从1开始，即下标0的值一定不是重复值
        int fast = 1;
        //慢指针，从1开始，即下标0的值一定不是重复值
        int slow = 1;
        //将fast指针一直向前移动，直到移动到下标nums.length-1
        while(fast < nums.length){
            //当fast与 fast-1(前一个元素)的值不等时，即不是重复元素，那就可以放到nums数组中
            //因为slow初始值是1，所以先直接赋值，然后将slow+1，即长度+1
            //最后将fast指针向前移动一位
            if(nums[fast] != nums[fast-1]){
                nums[slow] = nums[fast];
                slow++;
            }
            fast++;
        }
        //返回新数组长度
        return slow;
    }
}
