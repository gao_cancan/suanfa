package lianbiao.q3;

/**
 * 反转链表
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 思路：申请链表指针node，虚拟头指针newHead，遍历链表
 * 当node不为null，即没有遍历完。头指针node.next赋值为n，用于临时保存。
 * node.next指针指向虚拟头节点的next节点，再让虚拟头节点指向node节点，最后临时的n节点，赋值给node节点，用于下面的遍历。
 * 这里应该记住：虚拟头节点，永远指向头节点。
 * 虚拟头节点声明时，肯定链表为空，指向的是null（真正头节点），让node节点去指向真正头节点，不就是在表头插入节点，不就是倒叙插入。
 * 然后再让虚拟头节点，向前移动，指向新的真正的头节点。
 *
 * 错误的思路：1、我保存了临时节点n，但我让node.next 指向了newHead，这里就忘了newHead节点的next节点才是真的头节点，node.next应该
 * 去指向真正的头节点才对，这才是头插入，所以node.next = newHead.next。
 * 2、我用node.next 指向newHead后，我想newHead应该向前移动，怎么移动，newHead = node，就是向前移动了。这更是错误
 * 虚拟头节点指向真正的头节点，怎么算指向，newHead.next，才是指向，我把newHead向前移动一位，就变成了node节点，就不是指向真正的头
 * 节点。
 */


class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
public class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode node = head;
        ListNode newHead = new ListNode();
        while(node != null){
            //先保留node.next节点，用于之后的遍历
            ListNode n = node.next;
            //node节点指向虚拟头节点的next节点，然后虚拟头节点向前移动指向node节点，最后把临时n节点赋值给node节点，用于遍历
            node.next = newHead.next;
            newHead.next = node;
            node = n;
        }
        return newHead.next;
    }
}
