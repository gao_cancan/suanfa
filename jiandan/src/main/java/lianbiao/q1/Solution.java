package lianbiao.q1;

/**
 * 删除链表中的节点
 * 请编写一个函数，使其可以删除某个链表中给定的（非末尾）节点。传入函数的唯一参数为 要被删除的节点 。
 * 输入：head = [4,5,1,9], node = 5
 * 输出：[4,1,9]
 * 解释：给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 4 -> 1 -> 9.
 * 链表至少包含两个节点。
 * 链表中所有节点的值都是唯一的。
 * 给定的节点为非末尾节点并且一定是链表中的一个有效节点。
 * 不要从你的函数中返回任何结果。
 *
 * 思路：这个题有些不一样，刚开始没理解什么意思，如果要删除应该给head依次遍历，找到再删除。但这个题，意思是，参数是要删除的节点，
 * 那就直接去掉该节点即可。参数前面的节点，不需要管。
 * 把node节点的后一个节点的值，赋给node.value，然后再把node的next指针，指向node的next的next节点。这样就完成了对node节点的删除
 * 这里实际上是值替换，把node的next节点的value赋给node的value，这就算是node节点的删除，再把next指针指向node的next的next节点，
 * 就算完成了删除。
 */

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}
public class Solution {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
