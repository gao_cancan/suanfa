package lianbiao.q2;

/**
 * 删除链表的倒数第N个节点
 * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
 * 进阶：你能尝试使用一趟扫描实现吗？
 * 链表中结点的数目为 sz
 * 1 <= sz <= 30
 * 0 <= Node.val <= 100
 * 1 <= n <= sz
 */



/**
 * todo 有问题的解法
 * 链表翻转，然后删除目标节点，但是返不回头节点了。
 */
public class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        //先将链表翻转
        ListNode reverse = reverse(head);
        //声明变量k，用于倒数第几
        int k = 1;
        //声明虚拟头指针
        ListNode test = new ListNode();
        //开始遍历翻转后的链表reverse
        ListNode pre = reverse;
        while(pre != null){
            if(k == n){
                ListNode temp = pre.next;
                pre = temp;
            }
            k++;
            pre = pre.next;
        }
        return pre;
    }

    //链表翻转
    public ListNode reverse(ListNode head){
        ListNode pre = head;
        //虚拟头结点
        ListNode node = new ListNode();
        while(pre != null){
            //临时保存pre.next，用于下次遍历
            ListNode temp = pre.next;
            //pre节点指向虚拟头节点node的next节点（真正的头节点）
            pre.next = node.next;
            //虚拟头节点向前移动，指向现在的真正的头节点pre
            node.next = pre;
            //临时节点temp变成头节点（要遍历的链表头节点）
            pre = temp;
        }
        return node.next;
    }
}
