package lianbiao.q2;

/**
 *
 * 思路：先遍历链表，找到总长度k，然后两个指针pre、curr分别指向第一个和第二个节点（也是倒数第k个，第k-1个节点）
 * 1、当删除的是头节点，即n == k，直接返回第二个节点即可
 * 2、当不是头节点，两指针同时向后移动，curr最多移动到尾节点，即倒数第1个节点
 * 如果i == n，说明找到了要删除的节点curr，让pre节点指向curr的next节点即可
 * 如果i != n，说明没找到，两指针同时向后移动，并将i--，
 * 变量i表示curr指针指向倒数第i个节点，curr向后移动一位，i就要-1
 */
public class Solution1 {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode node = head;
        //k为链表长度
        int k = 0;
        while(node != null){
            node = node.next;
            k++;
        }
        //遍历完成，得到链表长度k
        //获取第一个节点和第二个节点
        ListNode pre = head;
        ListNode curr = head.next;
        //当n==k，表示要删除头节点，那第二个节点变成新的头节点，直接返回curr节点即可
        if(n == k){
            return curr;
        }

        //变量i表示curr指针指向倒数第i个节点，curr向后移动一位，i就要-1
        int i = k-1;
        while(pre != null && curr != null){
            //找到了，pre指针指向curr的next节点，并跳出循环
            if(i == n){
                pre.next = curr.next;
                break;
            }else{
                //没找到，两个指针继续向后移动，并将i--
                pre = pre.next;
                curr = curr.next;
            }
            i--;
        }
        return head;
    }
}
