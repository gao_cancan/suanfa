package lianbiao.q4;

/**
 * 合并两个有序链表
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 * 思路：1、如果其中一个为null则返回另一个
 * 2、申请节点pre1和pre2，分别指向两个链表的头节点，申请一个新的链表（虚拟头节点），node指针用于移动
 * 当两个链表都不为null时，去比较pre1和pre2指向的value的值，node指向其中一个节点，并将该链表向后移动，新链表节点也想后移动
 * 最后：其中一个为null则将指针指向不为null的链表即可。
 *
 * 我这里犯了一个错误，思路对，但我没有申请一个node节点用于新链表的移动，直接用pre一直在移动。
 */
class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        //如果l1链表为null，则直接返回l2
        if(l1 == null){
            return l2;
        }
        //如果l2链表为null，则直接返回l1
        if(l2 == null){
            return l1;
        }
        //分别申请两个链表的头指针
        ListNode pre1 = l1;
        ListNode pre2 = l2;
        //申请虚拟头节点
        ListNode pre = new ListNode(-1,null);
        //申请新链表的头指针，用于移动
        ListNode node = pre;
        //当pre1和pre2都不为null，比较它们的value
        while(pre1 != null && pre2 != null){
            //如果pre1.value < pre2.value，将新链表的头指针node指向pre1链表，然后将pre1指针后移一位，node指针后移一位
            if(pre1.val < pre2.val){
                node.next = pre1;
                pre1 = pre1.next;
                node = node.next;
            }else{
                //如果pre1.value >= pre2.value，将新链表的头指针node指向pre2链表，然后将pre2指针后移一位，node指针后移一位
                node.next = pre2;
                pre2 = pre2.next;
                node = node.next;
            }
        }
        //如果pre2链表遍历完了，则node节点指向pre1链表
        if(pre1 != null){
            node.next = pre1;
        }
        //如果pre1链表遍历完了，则node节点指向pre2链表
        if(pre2 != null){
            node.next = pre2;
        }
        //返回新链表的头节点
        return pre.next;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1,null);
        ListNode l2 = new ListNode(3,null);
        ListNode l3 = new ListNode(4,null);
        l1.next = l2;
        l2.next = l3;

        ListNode l4 = new ListNode(1,null);
        ListNode l5 = new ListNode(3,null);
        ListNode l6 = new ListNode(4,null);
        l4.next = l5;
        l5.next = l6;
        ListNode listNode = new Solution().mergeTwoLists(l1, l4);
        System.out.println(listNode);
    }
}
