# 顺序表



| 类名     | SequenceList                 |                                                            |
| -------- | ---------------------------- | ---------------------------------------------------------- |
| 构造方法 | SequenceList(int capacity)   | 创建容量为capacity的SequenceList对象                       |
| 成员方法 | public void clear();         | 空置线性表                                                 |
|          | public boolean isEmpty();    | 判断线性表是否为空，是true，否false                        |
|          | public T get(int i);         | 读取并返回线性表中的第i个元素的值                          |
|          | public int length();         | 获取线性表中元素的个数                                     |
|          | public T insert(int i, T t); | 在线性表的第i个元素之前，添加一个元素t                     |
|          | public void insert(T t);     | 向线性表中添加一个元素t                                    |
|          | public T remove(int i);      | 删除并返回线性表中的第i个数据元素                          |
|          | public int indexOf(T t);     | 返回线性表中首次出现的指定的数据元素的位序号，不存在返回-1 |
| 成员变量 | private T[] eles;            | 存储元素的数组                                             |
|          | private int N;               | 当前线性表的长度                                           |

