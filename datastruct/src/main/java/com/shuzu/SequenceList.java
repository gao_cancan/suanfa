package com.shuzu;

/**
 * 顺序表
 */
public class SequenceList<T> {
    //存储元素的数组
    private T[] eles;
    //当前线性表中元素的个数
    private int N;

    //构造方法
    public SequenceList(int capacity){
        eles = (T[]) new Object[capacity];
        N = 0;
    }

    //将线性表置为空表（长度置为0即为空表）
    public void clear(){
        N = 0;
    }

    //判断线性表是否为空，空true，不空false
    public boolean isEmpty(){
        return N == 0;
    }

    //获取线性表的长度
    public int length(){
        return N;
    }

    //获取指定位置的元素
    public T get(int i){
        if(i < 0 || i >= N){
            throw new RuntimeException("数组下标越界");
        }
        return eles[i];
    }

    //向线性表中添加元素t
    public void insert(T t){
        if(N == eles.length){
            throw new RuntimeException("当前线性表已满");
        }
        eles[N] = t;  //也可以eles[N++] = t;
        N++;
    }

    //在下标i处插入元素t
    public void insert(int i, T t){
        if(i == eles.length){
            throw new RuntimeException("当前线性表已满");
        }
        //数组是连续的，插入只能插入到N及以前位置
        if(i < 0 || i > N){
            throw new RuntimeException("插入的位置不合法");
        }
        //把下标i及之后的元素依次后移一位
        for (int j = N; j > i; j--) {
            eles[j] = eles[j-1];
        }
        //把t元素放到下标i处
        eles[i] = t;
        //元素数量+1
        N++;
    }

    //删除指定位置i处的元素，并返回该元素
    public T remove(int i){
        if(i < 0 || i > N-1){
            throw new RuntimeException("删除的下标越界|删除的元素不存在");
        }
        T temp = eles[i];
        //把下标i后面的元素，都向前移动一位
        for(int j = N-1; j > i; j--){
            eles[j-1] = eles[j];
        }
        //长度-1
        N--;
        return temp;
    }

    //查找t元素第一次出现的位置
    public int indexOf(T t){
        if(t == null){
            throw new RuntimeException("查找的元素不合法");
        }
        //遍历，比较值，相等则返回
        for (int i = 0; i < N; i++) {
            if(eles[i].equals(t)){
                return i;
            }
        }
        //没有相等的，返回-1
        return -1;
    }
}
